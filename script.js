// TASK 1

const btn = document.getElementById('btn-click');
const section = document.getElementById('content');

btn.addEventListener('click', (e) => {
  const p = document.createElement('p');
  p.textContent = 'New Paragraph';
  section.append(p);
});

// TASK 2

const inputBtn = document.createElement('button');
inputBtn.id = 'btn-input-create';
inputBtn.textContent = 'Create Input Field';
inputBtn.style.display = 'block';

section.append(inputBtn);

inputBtn.addEventListener('click', () => {
  const input = document.createElement('input');
  input.style.display = 'block';
  input.type = 'text';
  input.id = 'input-field';
  input.setAttribute('placeholder', 'Enter your name here');
  input.setAttribute('name', 'inputName');

  section.append(input);
});